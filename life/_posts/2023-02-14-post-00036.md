---
layout: post
title: "리니지M 무과금 암흑기사 신서버 육성기 - 사전예약과 신섭혜택"
toc: true
---

 리니지M의 신서버 케레니스가 오픈됐습니다. 며칠지나서 빠르게 렙업하는 유저분들도 많고 사람인지 매크로인지 많아서 북적거리는 모습이 본보기 좋네요. 당연히 사냥하기는 안좋지만말이죠. 신규 서버를 오픈하면 약 한달동안은 구서버와 다르게 아인하사드 버프와 드랍률 상승등의 혜택을 누릴수 있습니다. 드랍률은 머 큰차이 없지만 아인하사드 버프는 빠른 육성에 대단히 좋습니다.

## 1. 리니지M 신서버 케레니스의 혜택들

 신서버 골통이 접속하는 날에 이렇게 대기열이 생기더군요. 당기 제가 하는 서버는 농사섭(비인기/전투가 별로없는/사냥만하는) 이라서 대기없이 접속이가능합니다. 첫날은 그랬어요.


 암흑기사를 키우는데 처음이라 뭐가 뭔지 모르고 퀘스트만 앞서 따라갔습니다. 게다가 사전예약쿠폰 (지금은 발급되지않아요) 을 등록해서 옥쇄 (캐릭슬롯 단독 추가) 고급 7검에 4셋 기간제 티켓 (이건 제작-이벤트-상자에서 클래스 선택해서 제작가능해요.) 드래곤의 다이아몬드(아인하사드충전) 이렇게 지급받았습니다.

 신서버에서는 처음에 아데나(골드) 모으기도 힘들어서 그대로 사전예약 쿠폰이 있어야합니다. 맨땅에 해딩은 시방 힘들어서 하다가 포기하는 수준이구요. 어쨌건 이렇게 지급받은 장비를 착용하고 퀘스트를 진행합니다.


 그러다가 어째서 아인하사드 축복 버프가 없지? 드랍률은? 이렇게 생각했는데 자동으로 적용이 아니라서 "상점 - > 이벤트&강화 - > 신서버 독특 혜택 상자"를 구매해서 수동으로 적용(?)을 해야합니다. 아차 싶었네요. 그간 느리게 렙업했던걸 생각하면.... 아이구 이빡구야..


 신서버 혜택의로 이안하스드의 행운 물약을 먹으면 지속시간 30일동안 드랍률 2배 (아데나 제외). 발돋움 물약은 이것도 한달동안 경험치 보너스 500% 둘다 꿀이라서 으레 적용하세요. 아울러 45렙부터는 렙업마다 보상과 수확 퀘스트를 받을수 있어 우극 빠른 렙업이 가능합니다.
### 2.무과금 암흑기사 육성

 스킬은 기본으로 제공받은 스킬을 제외하고 "다크 아머" 스킬만 추가로 구매했습니다. 아데나가 모이면 "다크 임팩트"를 구매할 예정이구요.


 배운 스킬들과 효과들을 짧게 설명하면 위선 공통으로 1단계 마법을 배울수 있습니다. 마법상인에게 1단계 마법들 이소 배우세요 "쉴드(AC-2)"는 소요 스킬입니다. 라이트는 전투하시는 분들은 항상 안배우더라구요. 그건 취향차이니깐 뭐..


 암흑기사 스킬에서는 기본으로 지급받은 "다크니스 싸인" 물리공격력을 마법공격력으로 바꿔주는 스킬인데 패시브 스킬이라 기본적용입니다. 끄고 자시고없어요.  "다크 블레이드"는 검기? 그런 공격인데 사정거리가 늘어나지만 위력은 약해서 만분 쓰지 않습니다. "다크 소울"은 몬스터를 처치하면 일정 확률로 HP/MP를 회복시켜줍니다. 사용하고 있어요.
 거기에 113만 아데나를 주고 배운 "다크 아머"는 데미지 리덕션 +4 시켜주는 꿀 스킬이라서 필수로 쓰고있습니다. 몸빵이 더욱더욱 좋아져요. 이제 확률적으로 데미지를 올려주는 "다크 임팩트"를 구입해서 배워야합니다. 우선을 이렇게 스킬 구성해서 [리니지 프리서버](https://freenex.net) 쓰려구요.

 암흑기사 스텟은 말이 많았는데 으뜸 힘을 깔고 가는 것 같습니다. 어차피 물리 공격을 마법 공격으로 바꿔주니 힘을 최대치로 찍고 너 다음에 인트나 다른 능력치를 올립니다. 지금은 힘으로 속박 밀고 있어요.


 입때 육성은 무어 없고 퀘스트만 줄줄이 밀고 있습니다. 작업장들이 많은 사냥터는 매너모드를 끄고 에너지볼트 날리면서 한대씩만 때려서 퀘스트 클리어하고 (사이클롭스 퀘스트 같은 종류들 ) 나머지는 매너모드로 사냥돌리면서 퀘스트를 깨고 있습니다. 지금은 악어 200마리에서 멈추고 아데나 벌려고 사냥중이구요.

 정도 48에는 고급 변신카드들 수동레벨 52에는 고급변신 데스나이트를 받을 수 있으니 필야 52까지 빠르게 키워서 데스나이트 변신으로 사냥하시구요. 상점에서 변신카드 인형카드 뽑기가 있으니 하루에 5만/9만 뽑기도 집고 하셔야합니다. 희귀 변신이라도 나오면 더욱더 빠르게 사냥이 가능해서요.
 그럼 52레벨 이후로 아이템 맞추고 레벨업하는 내용으로 2편 포스팅하겠습니다. 신규서버 암흑기사 유저분들 전부 열렙 득템하세요 ~!!! :-D

 ▶ 다음 글 : 리니지M 무과금 암흑기사 신서버 육성기 2 - 부캐릭을 이용한 장비 맞추기
